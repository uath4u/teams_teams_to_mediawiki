#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import logging

from mwclient import Site
from pandas import DataFrame
from re import sub, MULTILINE, match
from os import path
from pathlib import Path


class TeamsToWiki:
    """Main Class to put information from teams to wiki

    Modules

    _get_token:                     get new bearertoken from graph-api
    get_teams_data:                 get groupdata from graph-api
    _get_owners:                    get the owneers of a group from graph-api
    _get_numbers_of_members:        get the count of groupmembers for a group from graph-api
    write_table_to_wiki:            writes the summarytable of groups to mediawiki
    _build_table:                   build the summarytable of groups

    Attributes

    self.header:                    Dictionary containing the Bearertoken to authenticate
    self.teams_dataframe:           pandas.DataFrame containing all teamsdata
    self.teams_ignore_list:         List containing all Teams witch should not be writen to wikitable
    """

    def __init__(self):
        """Instantiating Class TeamsToWiki

        self.teams_dataframe is setting up as an instance of pd.DataFrame()
        sel.header ist setting up as an instance of Dictionary containing the Bearertoken
        """

        try:

            self.header = {'Authorization': f'Bearer {self._get_token()}'}
            self.teams_dataframe = DataFrame()
            self.ignore_teams_list = self._get_teams_ignore_list()

        except Exception as exc:

            logging.error(f'Function {TeamsToWiki.__init__.__qualname__} raised error {exc}')
            exit(1)

    def _get_token(self):
        """Gets a new token from graph-api.

        _get_token() -> String

        Function

        Gets the bearertoken by calling graph-api and returns it as a string to caller.
        """

        try:

            logging.info('Getting Accesstoken')

            body = {'grant_type': 'client_credentials',
                    'client_secret': 'INSERT GRAPH API USER PASSWORD',
                    'scope': 'https://graph.microsoft.com/.default',
                    'client_id': 'INSERT GRAPH API USER ID'
                    }

            header = {'Host': 'login.microsoftonline.com',
                      'Content-Type': 'application/x-www-form-urlencoded'
                      }

            return requests.post('https://login.microsoftonline.com/INSERT TENANT/oauth2/v2.0/'
                                 'token', headers=header, data=body).json()['access_token']

        except Exception as exc:

            logging.error(f'Function {TeamsToWiki._get_token.__qualname__} raised error {exc}')
            exit(1)

    def get_teams_data(self):
        """Puts a new token to keepass.kdbx.

        get_teams_data(self) -> None

        Function

        Gets a list of all teams containing the name and the groupid for all teams and calls for every team in list the
        functions self_get_oweners and self._get_number_of_members and add the results to the list.
        After all, the list is converted to da pandas-dataframe sorted by teamnames.
        """

        try:

            logging.info('Getting all teams data')

            teams_list = []

            for content in requests.get("https://graph.microsoft.com/beta/groups?$filter=resourceProvisioningOptions/"
                                        "Any(x:x eq 'Team')&$select=id,displayName,description&$top=500",
                                        headers=self.header).json()['value']:

                if content['id'] not in self.ignore_teams_list:

                    content['owners'] = self._get_owners(content['id'])
                    content['number_of_members'] = self._get_number_of_members(content['id'])

                    teams_list.append(content)

            self.teams_dataframe = DataFrame(teams_list).sort_values(by='displayName')

        except Exception as exc:

            logging.error(f'Function {TeamsToWiki.get_teams_data.__qualname__} raised error {exc}')
            exit(1)

    def _get_owners(self, team_id):
        """get the the owners of a group identified by groupid

        _get_owners(self, team_id -> String) -> List of owners

        Arguments

        team_id     Group-ID for the group you want to get the owners

        Function

        Gets the owners by calling graph-api.
        """

        try:

            return [x['displayName'] for x in requests.get(f'https://graph.microsoft.com/v1.0/groups/{team_id}/owners',
                                                           headers=self.header).json()['value']]

        except Exception as exc:

            logging.error(f'Function {TeamsToWiki._get_owners.__qualname__} raised error {exc}')
            exit(1)

    def _get_number_of_members(self, team_id):
        """Get the number of members in a group

        get_emails(self, team_id -> String) -> Int (number of members)

        Arguments

        team_id     Group-ID for the group you want to get the

        Function

        Gets the number of members by calling graph-api.
        """

        try:

            return len(requests.get(f'https://graph.microsoft.com/v1.0/groups/{team_id}/members?$select=displayName'
                                    f'&$top=500', headers=self.header).json()['value'])

        except Exception as exc:

            logging.error(f'Function {TeamsToWiki._get_owners.__qualname__} raised error {exc}')
            exit(1)

    def write_table_to_wiki(self):
        """Writes all teamsdata to mediawiki

        get_emails(self) -> None

        Function

        Writes the table of teams build by _build_table() to mediawiki using REST-API.
        """
        try:

            logging.info('Writing table to wiki')

            site = Site('INSERT MEDIAWIKI URL')

            site.login(username='MEDIAWIKI USER NAME', password='MEDIAWIKI USER PASSWORD')

            page = site.pages['Liste aller Teams in MS Teams']

            page.edit(self._build_table())

            logging.info('Table was refreshed')

        except Exception as exc:

            logging.error(f'Function {TeamsToWiki.write_table_to_wiki.__qualname__} raised error {exc}')
            exit(1)

    def print_teams_dataframe(self):
        print(self.teams_dataframe.keys())

    @staticmethod
    def _get_teams_ignore_list():
        """Get the teams_ignore_list from team_ignore.cfg.

        get_teams_ignore_list() -> List

        Open team_ignore.cfg and return all necessary data.
        """

        with open(path.join(Path(__file__).resolve().parent, 'team_ignore.cfg')) as file:

            teams_ignore_data = file.readlines()

            return [ignore_team for ignore_team in teams_ignore_data if not match('#', ignore_team) and
                    ignore_team != '\n']

    def _build_table(self):
        """Creates a media-wiki-table out of self.teams_dataframe.

        get_emails(self) -> None

        Function

        Creates a media-wiki-table out of self.teams_dataframe.
        """

        try:

            logging.info('building wiki table')

            table = "{{:TC:Liste aller Teams in MS Teams}} \n\n" \
                    "{| class=\"wikitable sortable\"\n|+\n!'''Team'''\n!'''Beschreibung'''\n!'''Verantwortliche*r'''" \
                    "\n!'''Anzahl der Mitglieder'''"

            for team in self.teams_dataframe.itertuples():

                if team[5] > 1:

                    description = sub('^-', '', team[3], flags=MULTILINE).replace('\t', '')

                    table += f'\n|-\n|{team[2]}\n|{description}\n|{", ".join(team[4])}\n|{team[5]}'

            table += '\n|}\n\n[[Category:AutomatischerInhalt]]'

            return table

        except Exception as exc:

            logging.error(f'Function {TeamsToWiki._build_table.__qualname__} raised error {exc}')
            exit(1)


if __name__ == '__main__':

    logging.info('Starting script')

    inst = TeamsToWiki()
    inst.get_teams_data()
    inst.print_teams_dataframe()
    inst.write_table_to_wiki()

    logging.info('Script finished')
    exit(0)
