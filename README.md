# teams_teams_to_wikimedia

teams_teams_to_wikimedia is a script witch list all teams from teams on a
wikimedia side including the number of team members and the owners.

## Installation

Clone repository

git clone https://gitlab.com/uath4u/teams_teams_to_mediawiki.git

Install dependencies with pip.

pip3 install requirements.txt

## Configuration

Put all teams to teams_ignore.cfg witch you don't want to show up in mediawiki.

Search for

- MEDIAWIKI URL
- MEDIAWIKI USER NAME
- MEDIAWIKI USER PASSWORD

and replace it with your credentials, url.

Search for:

- GRAPH API USER PASSWORD
- GRAPH API USER ID

and replace it with your credentials.

Search for:

- TENANT

and replace it with your tenant.


## Usage

Start the script. The script tansfers all teams to the specified site in
mediawiki.
